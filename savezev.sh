#!/bin/bash
echo ""
echo "dump complet zev"
mongodump -d zev -o /opt/workspace/save_db/zev.json;
echo ""
echo "export events"
mongoexport -d zev -c events -o /opt/workspace/save_db/events.json;
echo ""
echo "export users"
mongoexport -d zev -c users -o /opt/workspace/save_db/users.json;
