const http = require("http");
const request = require("request");
const rp = require("request-promise");
const fs = require("fs");
const moment = require("moment");

//local
const ZEV_API_LOCAL = "http://localhost:3000/rest";
const ID_CATEGORIE_AUTRE_LOCAL = "5a78d33ed4eb3986d01536a8";
//sur zev
const ZEV_API = "https://zevenement.fr/rest";
const ID_CATEGORIE_AUTRE = "5bf4a1e9eb718830c1dc4ca1";
//sur local
// const ZEV_API = ZEV_API_LOCAL;
// const ID_CATEGORIE_AUTRE = ID_CATEGORIE_AUTRE_LOCAL;
//https://data.toulouse-metropole.fr/explore/dataset/agenda-des-manifestations-culturelles-so-toulouse/information/
const OPEN_API =
  "https://data.toulouse-metropole.fr/api/records/1.0/search//?dataset=agenda-des-manifestations-culturelles-so-toulouse&rows=9999&sort=-date_debut&facet=type_de_manifestation";



const getEvents = async () => {
  return await rp({
    uri: `${ZEV_API}/event`,
    method: "GET",
    json: true
  });
};

const getOpenAPI = async () => {
  return await rp({
    uri: `${OPEN_API}`,
    method: "GET",
    json: true
  });
};
const getCommune = async () => {
  return await rp({
    uri: `${ZEV_API}/commune`,
    method: "GET",
    json: true
  });
};

const postEvent = async (jwtToken, event) => {
  console.log("postEvent", event);
  return await rp({
    uri: `${ZEV_API}/event`,
    method: "POST",
    headers: {
      Authorization: jwtToken,
      "Content-Type": "application/json"
    },
    json: event
  });
};

const run = async () => {
  try {
    let jwtToken = "";

    // recup des categories
    const categories = async () => {
      return await rp({
        uri: `${ZEV_API}/category`,
        method: "GET",
        json: true
      });
    };

    // recup des tags
    //const tags = await rp({ uri: `${ZEV_API}/tag`, method: "GET", json: true });

     //jwtToken = await rp({uri: `${ZEV_API_LOCAL}/user/authenticate`, method: 'GET', json: userAdminLocal});
     jwtToken ="JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1YmY0YTQ4NmU3OWY0MjJhYTUwNGU5NjYiLCJleHBpcmVUaW1lIjoxNTcwMDA0MDkxODI0fQ.KvkSWZGOJqPdrItUPfrjQ3iuSCjYPku9Gl_7fdQQYi0";
   
     getOpenAPI().then(results => {
      console.log("NOMBRE DE RESULTATS : ",results.records.length)

      results.records.forEach((result, index) => {
        let responseOpenApi = result.fields;
        console.log(index)
        indexsortie=index;
        
        let datetestStart = moment(responseOpenApi.date_debut);
        let datetestEnd = moment(responseOpenApi.date_fin);
        let today = moment(new Date());
        let sameYear = datetestStart.isSame(today, 'year') &&  datetestEnd.isSame(today, 'year') ;
        if (datetestStart && datetestEnd ) {
          if (datetestStart.isAfter() && datetestEnd.isAfter() && sameYear) {
        
        // propriétés communes à tous les events
        const event = {
          contact: {
            annonceMail: "",
            annoncePhone: "",
            email: "contact@zevenement.fr",
            phone: "",
            webSite: ""
          },
          title: "",
          id_external: "",
          description: "",
          dates: [{}],
          prive: false,
          public: true,
          permanent: false,
          ponctuel: true,
          promotion: false,
          inscriptionTicket: false,
          organisateur: false,
          lieu: "",
          adresse: "",
          inscription: false,
          nbplaces: 0,
          outdoor: true,
          horaires: "",
          status: "published",
          price: 0,
          price_string:"",
        };

        // id
        if (responseOpenApi.identifiant) {
          event.id_external = responseOpenApi.identifiant;
        }
        // name
        if (responseOpenApi.nom_de_la_manifestation) {
          event.name = responseOpenApi.nom_de_la_manifestation;
        }
        // email
        if (responseOpenApi.reservation_email) {
          event.email = responseOpenApi.reservation_email;
        }
        // phone
        if (responseOpenApi.reservation_telephone) {
          event.phone = responseOpenApi.reservation_telephone;
        }
        // siteweb
        if (responseOpenApi.reservation_site_internet) {
          event.siteweb = responseOpenApi.reservation_site_internet;
        }
        // title
        if (responseOpenApi.nom_de_la_manifestation) {
          event.title = responseOpenApi.nom_de_la_manifestation;
        }
        // description
        if (responseOpenApi.descriptif_court) {
          event.description = responseOpenApi.descriptif_court;
        }
        // lieu
        if (responseOpenApi.lieu_nom) {
          event.lieu = responseOpenApi.lieu_nom;
        }

        // adresse
        if (responseOpenApi.lieu_adresse_2) {
          event.adresse = responseOpenApi.lieu_adresse_2;
        }

        // horaire : dates.infos
        if (responseOpenApi.horaires) {
          event.dates[0].infos = responseOpenApi.horaires;
        }
        // datedebut
        event.dates[0].datedebut = moment(responseOpenApi.date_debut).format(
          "YYYY-MM-DD"
        );
        event.dates[0].datefin = moment(responseOpenApi.date_fin).format(
          "YYYY-MM-DD"
        );

        // enfant
        if (responseOpenApi.tranche_age_enfant) {
          event.enfant = true;
        }

        // price : 1 gratuit sur 2 - 1 à 100 € si payant
        if (
          !responseOpenApi.tarif_normal ||
          responseOpenApi.manifestation_gratuite
        ) {
          event.price = 0;
        } else {
          if (responseOpenApi.tarif_normal) {
            // de type merdique : "De 3,00 € à 5,00 €"
            let testTypePrice =responseOpenApi.tarif_normal.replace(",", ".").replace("€", "").replace(' ','');
            if (!parseFloat(testTypePrice)) {
              testTypePrice =  testTypePrice.split("De").join('De ').split(".00").join('').split(".").join(',');
              event.price_string = testTypePrice;      
                    }else{                  
                  event.price = parseFloat(responseOpenApi.tarif_normal);
                }
          }
        }

        // commune

        //commune par default
        event.commune = {
          codeCom: "31555",
          codeDep: "",
          codeReg: "76",
          id: "5a78d33dd4eb3986d014d659",
          loc: [1.43229743595, 43.5959710776],
          nomCom: "NON DEFINIE",
          nomDep: "HAUTE-GARONNE",
          nomReg: "OCCITANIE",
          population: 453317
        };

        // on tente de matcher avec les communes de notre bd
        let asyncGetCommune = async () => {
          await getCommune().then(communes => {
            for (let i = 0; i < communes.length; i++) {
              if (communes[i].nomCom === responseOpenApi.commune) {
                event.commune = communes[i];
                //console.log("match =", event.commune)
                break;
              }
            }
          });

          // loc
          const x = responseOpenApi.geo_point[1];
          const y = responseOpenApi.geo_point[0];
          event.loc = [x, y];

          // category
          let matches = ["ulture", "port", "usique", "oncert", "xpo"];
          let match = false;
          for (let i = 0; i < matches.length; i++) {
            await categories().then(cate => {
              // tente de faire un match entre les catégories pour faire l'association
              // sinon affecter categorie = 'Autre'
              var strRegExPattern = matches[i];
              for (let i = 0; i < cate.data.length; i++) {
                if (responseOpenApi.categorie_de_la_manifestation) {
                  if (
                    responseOpenApi.categorie_de_la_manifestation
                      .toLowerCase()
                      .includes(strRegExPattern)
                  && cate.data[i].name.toLowerCase()
                  .includes(strRegExPattern)) {
                    event.category = cate.data[i];
                    match = true;
                    console.log("match catégorie entre : ", responseOpenApi.categorie_de_la_manifestation, " et :",cate.data[i].name);
                    break;
                  }
                }
              }
            });
          }
          if (!match) {
            event.category = { id: ID_CATEGORIE_AUTRE, name: "Autre" };
            console.log("pas de match catégorie");
          }

          // creation de l'event

          await getEvents().then(results => {
            returnValue = true;
            if (results.data.nbTotalResults > 0) {
              results.data.events.forEach(res => {
                if (res.id_external == event.id_external) {
                  console.log("NE PAS INSERER UN DOUBLON");
                  returnValue = false;
                }
              });
            }

            if (returnValue) {
              rp({
                uri: `${ZEV_API}/event`,
                method: "POST",
                headers: {
                  Authorization: jwtToken,
                  "Content-Type": "application/json"
                },
                json: event
              });
            }
          });

          
        };
        asyncGetCommune();
    }
}
      });
     

    });
  } catch (e) {
    console.error(e);
  }
};

run();
