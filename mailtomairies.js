const http = require("http");
const request = require("request");
const rp = require("request-promise");
const fs = require("fs");
const nodemailer = require("nodemailer");
var handlebars = require('handlebars');
let serverConfig = {
  mailFrom: (string =
    process.env.ZEV_MAIL_FROM || '"Zevenement" <contact@zevenement.fr>'),
  mailZev: (string = process.env.ZEV_MAIL_ZEV || "contact@zevenement.fr"),
  mailPass: (string = process.env.ZEV_MAIL_PASS || "Wcvruxpv01/"),
  mailSmtp: (string = process.env.ZEV_MAIL_SMTP || "pro1.mail.ovh.net"),
  mailSmtpPort: (number = parseInt(process.env.ZEV_MAIL_SMTP_PORT) || 587),
  mailprivateKey: (string =
    process.env.ZEV_MAIL_PRIVATE_KEY ||
    "MIIEpAIBAAKCAQEAxLnCh3yYC/DWoyM2DwjtS7ZmKjNV71GO7ce1hfg6G4m7YgkK418IE6+RbeSsj3a4oEnPD5drZQbv7a4Sr3rmCrXoUVpQbSCFlbptnrAKoeoNGq65tA6IdOer0RBPQ/U6nKE41T2FMxQDz+Gg0V8ilg02xDPzQXVy+HaW6m5YrA7EWWdnRpVB/JLVXgIJsqsbp5OFdOE9hpJSrLpJbweh6ExC1HRXCrds2wkIpTrBZsoXTCRIcl5MiQdZBu/umq9DK2aQh9F283gj4WBgvsVs+kxPI1eJ0FIK+jmvGKg/z1kHO0lbLJWj3c9xhAz17t/igs/Sz13seTWBNzHTwXXesQIDAQABAoIBAD7S18Wuy1uc2N6WQxBAGI/COxQk+r+Hey6CRXz4DY53yKiBHMIJuimXOskuatER59oAWCixgXFs6rgri/oQNGCdQQih3+dJqH9tXs57h5m3heHMhK90qK00wtDr9XRNXx9f+SdYEy6BqMn9Uv8p84CZzbCQqpFv8XxZWs6Y2KZxLTIQduCSlptbrjeJRxp1Ojriw8fAMbbDgHmMPJxHjd6g/r2vswnEiBTgObhbvz6RogBG05VdwhdQ0k6LeLGq1SY9G9KtO+Ed0CiHsy80P3FvN/xv0C9/bdfvO5kvZyxh1J0vEnHCNGDNtVnSeSpRp3ZssCLThkoNDOG44F+DSEECgYEA7xWcSy60dGvkgLYmTyuM+b9i4mNHhcsr1XEqHHq6ahqt/tRAJMI91Jdv5iUcFXegIbWPk7YEYe2q8NU+9eqtPWQQljw3PEwjdMdSC/9qZhfHkNFibR3EopsxI/LVY4O0hg5f4r+41IlS15NwiMYsrK5OgLkfKmmnQnjyLN60RVkCgYEA0qTuNz9bVtsbsD64PHtlQ619T2ixAF17USxKBixPJhbaEyN2fw59N52I+0DsqRezokQdsSZDMkKSOC3//5WncrbnxKYtsHRBIliM1g0lDVjdmkCwRqYa5r79vH+B5+f3IXLuS1c9ry9GxGPci7qXX5BrwuUQqsbL6zb60nzLwRkCgYEA1pwDa8lE2qxutMrMoIxrQ1P0o1qSvRqfAj1Om7ne05eXUAOegGCLt+Un4OK2zt5Os3OSqfjc/jqlEwGYPo6la1IDQZhTzKBbw1uX3oUrMPFvhiMtwtzRwVSlS6uSuH1k8mC82YFXnBCYeEI05dFeELN2bD3AKAYmyZhfar+N+iECgYAS4TmAtisHo+fdaiG1OhIfeMNMhMOolrhg1ClmWD5X2aB+KqWKSdArVfFbI7ySg09Ucep/YECRlqnoYycYz18MxxwK4iIiOKlF3M3yuYbipV5nXvjtvCGZIPE5HeyUzpO16ck184HR29jp7VFtLLI4nIcsFr/hrqPEV08v4oOYsQKBgQC7a+NIXpvgs+pR29eAByeoTLZg41qgsKlepVhmSjM/43cJBtEpBFRmxtWOZkwqFzwk01BKHOlgVRS+Af1Ym/N/ah81T6S7rH/Az4+8RTxrygPpWqQquQOYxzTq59bPuV6W3FW5uM/MfjwAuDW5EUFVos0IouFbk/p7/j1hPJaZHw==")
};

let transporter = nodemailer.createTransport({
  host: serverConfig.mailSmtp,
  port: serverConfig.mailSmtpPort,
  secure: false, // use TLS
  dkim: {
    domainName: "zevenement.fr",
    keySelector: "2019",
    privateKey: serverConfig.mailprivateKey
  },
  auth: {
    user: serverConfig.mailZev,
    pass: serverConfig.mailPass
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
});

const sendWithNodeMailer = async function(nodeMailerObject) {
  // envoie de mail
  // le contenu se trouve dans les template ejs
  console.log("envoie du mail ", nodeMailerObject);
  await transporter.sendMail(nodeMailerObject);
};

// create reusable transporter object using the default SMTP transport
let transporterTest = nodemailer.createTransport({
  host: serverConfig.mailSmtp,
  port: serverConfig.mailSmtpPort,
  secure: false, // true for 465, false for other ports
  auth: {
    user: serverConfig.mailZev,
    pass: serverConfig.mailPass
  }
});

// verify connection configuration
transporterTest.verify(function(error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log("Server is ready to take our messages");
  }
});

var readHTMLFile = function(path, callback) {
  fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
      if (err) {
          throw err;
          callback(err);
      }
      else {
          callback(null, html);
      }
  });
};

// envoi email
let mails = require('./mail.js').mailsMairies;
readHTMLFile('./template.html', function(err, html) {
  var template = handlebars.compile(html);
  var replacements = {
       username: "John Doe"
  };
  var htmlToSend = template(replacements);



  let envoieMail = () => {
    // envoie de mail
    let mailOptions = {
      to: mails,
      from: serverConfig.mailFrom,
      subject:
        "zevenement.fr - mise en ligne ",
      text: "",
      html: htmlToSend
    };
    console.log("envoie des mails ");
    console.log("--------------------------------------------");
    console.log("--------------------------------------------");
    console.log("--------------------------------------------");

   transporter.sendMail(mailOptions);
  };

// le seul moyen trouvé pour faire un vrai timeout :
//https://stackoverflow.com/questions/15682524/settimeout-in-node-js-loop/30575612#30575612
// var counter = mails.length-1;
// function makeRequest(i) {
//   envoieMail(i);
// }
// function timer() {
//   console.log(counter);
//   makeRequest(counter);
//   counter--;
//   if (counter >= 0) {
//       setTimeout(timer, 5000);    
//   }
// }
// timer();
envoieMail();

  
});
