const http = require('http');
const request = require('request');
const rp = require('request-promise');
const fs = require('fs');
const moment = require('moment');


 const ZEV_API = 'http://localhost:8080/rest';
 const users = [{
     email: 'zev@gmail.com',
     password: 'zevzev00',
 }];




const nbEvents = 100000;


const nomCommunes = JSON.parse(fs.readFileSync('jsons/nomCommunes.json', 'utf8'));
const words = JSON.parse(fs.readFileSync('jsons/words.json', 'utf8'));

const getRandom = (array) => {
    return array[Math.floor(Math.random() * array.length)];
}

const getRandomInteger = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

const getRandomFloat = (min, max) => {
    return (Math.random() * (max - min) + min);
}

const getRandomBoolean = () => {
    return Math.random() > 0.5;
}

const getRandomCommune = async () => {
    let commune = null;
    while (!commune) {
        let nomCommune = getRandom(nomCommunes);
        const communes = await rp({
            uri: `${ZEV_API}/commune`,
            method: 'GET',
            json: true,
            qs: {
                nom: nomCommune,
            }
        });
        if (communes.length > 0) {
            commune = communes[0];
        }
    }
    return commune;
}

const run = async () => {

    try {


        const jwtTokens = [];

        // recup des categories
        const categories = (await rp({uri: `${ZEV_API}/category`, method: 'GET', json: true,})).data;

        // recup des tags
        const tags = await rp({uri: `${ZEV_API}/tag`, method: 'GET', json: true,});

        // recup des tokens de tous les users
        for (const user of users) {
            const res = await rp({uri: `${ZEV_API}/user/authenticate`, method: 'POST', json: user});
            jwtTokens.push(res.data);
        }


        console.log('jwtTokens', jwtTokens);

        for (let i = 0; i < nbEvents; i++) {
            console.log(i);

            // propriétés communes à tous les events
            const event = {
                "contact": {
                    "name": "ssasa",
                    "email": "01235.678@sasa.sassa",
                    "phone": "0123456789"
                },
                "id": "creation",
                "dates": [{}],
                "prive": false,
                "public": true,
                "permanent": false,
                "ponctuel": true,
                "promotion": true,
                "inscriptionTicket": false,
                "organisateur": true,
                "lieu": "Bar Saint des saints",
                "adresse": "Saint Pierre des cuisines",
                "inscription": true,
                "nbplaces": 8,
                "outdoor": true,
                "status": "published"
            };

            // datedebut date au hasard dans la prochaie année
            const nbSecondsToAdd = Math.random() * 365 * 24 * 60 * 60;
            const momentDatedebut = moment().add(nbSecondsToAdd, 'seconds')
            const datedebut = momentDatedebut.format('YYYY-MM-DD[T]HH:mm:ss[.000Z]')
            event.dates[0].datedebut = datedebut;
            const momentDateFin= momentDatedebut.add(1, 'days');
 	    const datefin = momentDateFin.format('YYYY-MM-DD[T]HH:mm:ss[.000Z]')
            event.dates[0].datefin = datefin;

            // enfant
            event.enfant = getRandomBoolean();

            // price : 1 gratuit sur 2 - 1 à 100 € si payant
            if (getRandomBoolean()) {
                event.price = 0;
            } else {
                event.price = getRandomInteger(1, 100);
            }

            // commune
            event.commune = await getRandomCommune();

            // loc
            const x = event.commune.loc[0] + getRandomFloat(-0.1, 0.1);
            const y = event.commune.loc[1] + getRandomFloat(-0.1, 0.1);
            event.loc = [x, y];

            // category
            event.category = getRandom(categories);

            // tags
            event.tags = [getRandom(tags)];

            // title
            const nbWordsTitle = getRandomInteger(1, 6);
            let title = '';
            while(title.length < 3){
              for (let i = 0; i < nbWordsTitle; i++) {
                  title += getRandom(words) + ' ';
              }
            }
            event.title = title;

            // description
            const nbWordsDescription = getRandomInteger(0, 50);
            let description = '';
            for (let i = 0; i < nbWordsDescription; i++) {
                description += getRandom(words) + ' ';
            }
            event.description = description;

            // recup d'un jwttoken au hasard
            const jwtToken = getRandom(jwtTokens);

            // creation de l'event
            await rp({
                uri: `${ZEV_API}/event`,
                method: 'POST',
                headers: {
                    'Authorization': jwtToken,
                    'Content-Type': 'application/json'
                },
                json: event
            });
        }
    } catch (e) {
        console.error(e);
    }
}

run();
