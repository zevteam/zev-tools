const http = require("http");
const request = require("request");
const rp = require("request-promise");
const fs = require("fs");
const moment = require("moment");
const FuzzySet = require("fuzzyset");
// to test params:
// https://public.opendatasoft.com/explore/dataset/evenements-publics-openagenda/api/?disjunctive.keywords_fr&disjunctive.location_city&disjunctive.location_department&disjunctive.location_region&disjunctive.location_countrycode&refine.location_department=Haute-Garonne&refine.firstdate_begin=2023&refine.location_region=Occitanie&refine.location_city=Toulouse

//local
// const ZEV_API_LOCAL = "http://localhost:3000/rest";
// const ID_CATEGORIE_AUTRE_LOCAL = "677c03623c127b2d9102fd8d";
// const ZEV_API = ZEV_API_LOCAL;
// const ID_CATEGORIE_AUTRE = ID_CATEGORIE_AUTRE_LOCAL;
//sur zev prod
const ZEV_API = "https://zevenement.fr/rest";
const ID_CATEGORIE_AUTRE = "5e8f1c565055b74e03b934e6";


// reponse type api :
// "city":"Strasbourg",
// "date_start":"2017-09-29",
// "uid":"63414582",
// "pricing_info":"Entrée libre",
// "tags":"festival,musica,rencontre,strasbourg",
// "title":"L’exil, paroles et musiques",
// "image":"http://cibul.s3.amazonaws.com/event_l-exil-paroles-et-musiques_544366.jpg",
// "description":"rencontre",
// "updated_at":"2017-08-25T15:16:54+00:00",
// "space_time_info":"Bibliothèque nationale et universitaire de Strasbourg, le vendredi 29 septembre à 12:30",
// "timetable":"2017-09-29T12:30:00 2017-09-29T13:30:00",
// "image_thumb":"http://cibul.s3.amazonaws.com/evtbevent_l-exil-paroles-et-musiques_544366.jpg",
// "link":"http://openagenda.com/event/l-exil-paroles-et-musiques",
// "free_text":"Animée par Arnaud Merlin, producteur à France Musique Entendre, et faire entendre les voix de l’exil, dans un vaste mouvement choral porté par les jeunes acteurs-chanteurs de la Compagnie Sans Père : tel est le propos de ce spectacle imaginé par Sonia Wieder-Atherton. La violoncelliste s’explique sur ses choix de textes (de la Bible au génocide rwandais) et de musiques (de Purcell à nos jours), ainsi que sur sa collaboration avec Sarah Koné, Jean Kalman et Franck Krawczyk. Plus d'infos : [ici](http://www.festivalmusica.org/manifestation/1482/lexil-paroles-et-musiques)",
// "address":"6 Place de la République, 67000 Strasbourg",
// "department":"Bas-Rhin",
// "placename":"Bibliothèque nationale et universitaire de Strasbourg",
// "region":"Grand Est",
// "date_end":"2017-09-29",
// "lang":"fr"
// },
// "geometry":{
// "type":"Point",
// "coordinates":[
// 7.755881,
// 48.586978
// ]
// },
// "record_timestamp":"2017-08-25T15:16:54+00:00"
// },
function shuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
const userAdmin = {
  email: "contact@zevenement.fr",
  password: "zevzev00"
};
const userAdminLocal = {
  email: "moi@gmail.com",
  password: "Wcvruxpv01/"
};

const postEvent = async (jwtToken, event) => {
  // console.log("postEvent", event);
  return await rp({
    uri: `${ZEV_API}/event`,
    method: "POST",
    headers: {
      Authorization: jwtToken,
      "Content-Type": "application/json"
    },
    json: event
  });
};
let listodevent = [];
//let jwtToken = async () => await rp({uri: `${ZEV_API_LOCAL}/user/authenticate`, method: 'GET', json: userAdminLocal});
let jwtToken = "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1YmY0YTQ4NmU3OWY0MjJhYTUwNGU5NjYiLCJleHBpcmVUaW1lIjoxNzM2MjA2MDE2NjQ1fQ.ekIQCqIPNbmIDLpx61uWStuaTrspIEfayuJtFiBlxRM"

const run = async () => {
  try {
  

    // recup des communes
    console.log("communes");
    const communes = await rp({
      uri: `${ZEV_API}/commune?limit=-1`,
      method: "GET",
      json: true
    });
    // recup des categories
    console.log("categories");
    const categories = (await rp({
      uri: `${ZEV_API}/category`,
      method: "GET",
      json: true
    })).data;
    console.log("eventsZev");
    const eventsZev = (await rp({
      uri: `${ZEV_API}/event?noLimit=zevenementscesttropdelaballe`,
      method: "GET",
      headers: {
        Authorization: jwtToken,
        "Content-Type": "application/json"
      },
      json: true
    })).data;

    let departementsTest = [
      "Haute-Garonne",
    ];

    let departements = [
      "Bouches-du-Rhône",
      "Haute-Garonne",
      "Nord",
      "Gironde",
      "Seine-Saint-Denis",
      "Loiret",
      "Hauts-de-Seine",
      "Seine-Maritime",
      "Loire-Atlantique",
      "Ille-et-Vilaine",
      "Paris",
      "Bas-Rhin",
      "Yvelines",
      "Essonne",
      "Vienne",
      "Isère",
      "Finistère",
      "Maine-et-Loire",
      "Calvados",
      "Indre-et-Loire",
      "Val-d'Oise",
      "Métropole de Lyon",
      "Hérault",
      "Seine-et-Marne",
      "Pas-de-Calais",
      "Moselle",
      "Meurthe-et-Moselle",
      "Val-de-Marne",
      "Loire",
      "Charente-Maritime",
      "Drôme",
      "Alpes-Maritimes",
      "Marne",
      "Morbihan",
      "Côte-d'Or",
      "Puy-de-Dôme",
      "Var",
      "Saône-et-Loire",
      "Ain",
      "Oise",
      "Tarn",
      "Sarthe",
      "Gard",
      "Vaucluse",
      "Haute-Savoie",
      "Haut-Rhin",
      "Rhône",
      "Somme",
      "Eure",
      "Vendée",
      "Savoie",
      "Côtes-d'Armor",
      "Manche",
      "Pyrénées-Atlantiques",
      "Vosges",
      "Dordogne",
      "Yonne",
      "Deux-Sèvres",
      "Doubs",
      "Aube",
      "Aisne",
      "Aveyron",
      "Haute-Marne",
      "Charente",
      "Nièvre",
      "Mayenne",
      "Loir-et-Cher",
      "Pyrénées-Orientales",
      "Haute-Vienne",
      "Ardèche",
      "Eure-et-Loir",
      "Orne",
      "Cher",
      "Lot-et-Garonne",
      "Aude",
      "Landes",
      "Hautes-Pyrénées",
      "Jura",
      "La Réunion",
      "Meuse",
      "Gers",
      "Lot",
      "Corrèze",
      "Allier",
      "Ardennes",
      "Creuse",
      "Haute-Saône",
      "Seine-St.-Denis",
      "Alpes-de-Haute-Provence",
      "Haute-Loire",
      "Indre",
      "Tarn-et-Garonne",
      "Bouches-du-Rhone",
      "Guadeloupe",
      "Hautes-Alpes",
      "Ariège",
      "Haute-Corse",
      "Cantal",
      "Martinique",
      "Guyane"
    ];

    let nbrows = 100;

    // pour tester :
    // departements = departementsTest;
    //  nbrows = 50;

    // pour diffuser equitablement entre les departements
    // on prend un evenement par departement seulement
    let today = moment(new Date());

    console.log("today = " + today);

    indice_req = 0;
    // on cherche juste les 3 mois à venir
    const moisavenir = [
      today.format("YYYY-MM"),
      today.add(1, "months").format("YYYY-MM"),
      today.add(2, "months").format("YYYY-MM"),
      today.add(3, "months").format("YYYY-MM"),
      today.add(4, "months").format("YYYY-MM")
    ];

    for (let mois = 0; mois < moisavenir.length; mois++) {
      for (
        let indice_dept = 0;
        indice_dept < departements.length;
        indice_dept++
      ) {

        let OPEN_API =
          "https://public.opendatasoft.com/api/explore/v2.1/catalog/datasets/evenements-publics-openagenda/records?lang=fr&rows=" +
          nbrows +
          "&refine=location_department:" +
          departements[indice_dept] +
          "&refine=firstdate_begin:" +
          moisavenir[mois];
        const getOpenAPI = async () => {
          return await rp({
            uri: `${OPEN_API}`,
            method: "GET",
            json: true
          });
        };
        console.log(OPEN_API)
        console.log("departement :", departements[indice_dept]);

        await getOpenAPI().then(results => {
          console.log("NOMBRE DE RESULTATS : ", results.results.length);

          results.results.forEach((result, index) => {
            let responseOpenApi = result;
            console.log("index results.records :", index);
            indexsortie = index;

            // ne prendre que les events futur et de la meme année
            //let datetestStart = moment(responseOpenApi.firstdate_begin);

            //if (datetestStart) {
            // if (datetestStart.isAfter()) {
            // propriétés communes à tous les events
            const event = {
              contact: {
                annonceMail: "",
                annoncePhone: "",
                email: "contact@zevenement.fr",
                phone: "",
                webSite: ""
              },
              category: { id: ID_CATEGORIE_AUTRE, name: "Divers", "typeCate": "evenement" },
              title: "",
              id_external: "",
              description: "",
              dates: [{}],
              tags: [],
              image_url: "",
              prive: false,
              public: true,
              permanent: false,
              ponctuel: true,
              promotion: false,
              inscriptionTicket: false,
              organisateur: false,
              lieu: "",
              adresse: "",
              inscription: false,
              nbplaces: 0,
              // outdoor: false,
              status: "published",
              price: 0,
              price_string: ""
            };

            // horaires
            // if (responseOpenApi.timings && responseOpenApi.timings!="") {
            //   event.dates[0].infos = responseOpenApi.space_time_info;
            // }

            // dates
            if (responseOpenApi.timings) {
              let timings = JSON.parse(responseOpenApi.timings);

              // datedebut
              event.dates[0].datedebut = responseOpenApi.firstdate_begin;
              event.dates[0].datefin = responseOpenApi.firstdate_end;

            }



            if (event.dates[0].datefin == undefined) {
              event.dates[0].datefin = event.dates[0].datedebut;
            }

            console.log("------------------------------------------------------")
            console.log(event.dates[0].datedebut + "  " + event.dates[0].datefin)



            // id au niveau parent
            if (responseOpenApi.uid) {
              event.id_external = responseOpenApi.uid;
            }

            // tags
            if (responseOpenApi.keywords_fr) {
              event.tags = responseOpenApi.keywords_fr;
            }
            // image_url
            if (responseOpenApi.image) {
              if (responseOpenApi.image.split("https").length == 0) {
                event.image_url = responseOpenApi.image.replace(
                  "http",
                  "https"
                );
              } else {
                event.image_url = responseOpenApi.image;
              }
            }
            if (responseOpenApi.registration) {
              let registration = JSON.parse(responseOpenApi.registration);

              registration.forEach((reg, index) => {
                if (reg.type == "link") {
                  event.siteweb = reg.value;
                }


              });
            }

            // title
            if (responseOpenApi.title_fr) {
              event.title = responseOpenApi.title_fr;
              if (event.title.length > 127) {
                event.title = event.title.substring(0, 127);
              }
            }
            // description
            if (responseOpenApi.longdescription_fr && responseOpenApi.longdescription_fr != "") {
              event.description = responseOpenApi.longdescription_fr;
            } else if (responseOpenApi.description_fr) {
              event.description = responseOpenApi.description_fr;
            }
            // lieu
            if (responseOpenApi.location_name) {
              event.lieu = responseOpenApi.location_name;
            }

            // adresse
            if (responseOpenApi.location_address) {
              event.adresse = responseOpenApi.location_address;
            }

            // price : 1 gratuit sur 2 - 1 à 100 € si payant
            if (!responseOpenApi.conditions_fr || responseOpenApi.conditions_fr == "") {
              event.price = 0;
            } else {
              event.price_string = responseOpenApi.conditions_fr;
            }

            // commune
            // on tente de matcher avec les communes de notre bd


            for (let i = 0; i < communes.length; i++) {
              if (responseOpenApi.location_city) {
                if (
                  communes[i].nomCom.toLowerCase() ===
                  responseOpenApi.location_city.toLowerCase()
                ) {
                  event.commune = communes[i];
                  console.log("match commune=", event.commune.nomCom);
                  // loc
                  if (responseOpenApi.geometry) {
                    const x = responseOpenApi.location_coordinates.lon;
                    const y = responseOpenApi.location_coordinates.lat;
                    event.loc = [x, y];
                    // console.log(event.loc)
                  } else {
                    event.loc = [
                      event.commune.loc[0],
                      event.commune.loc[1]
                    ];
                    // console.log(event.loc)
                  }
                }
              }
            }

            // obligatoire
            if (event.commune && event.loc) {
              // category
              let matches = event.tags;
              let flag_match = false;

              for (let i = 0; i < matches.length; i++) {
                // tente de faire un match entre les catégories pour faire l'association
                // sinon affecter categorie = 'Divers'
                var strRegExPattern = matches[i].toLowerCase();
                for (let c = 0; c < categories.length; c++) {
                  let souscate = categories[c].name.split("/");
                  if (souscate) {
                    for (let s = 0; s < souscate.length; s++) {
                      fuse = FuzzySet();
                      fuse.add(souscate[s].toString());
                      if (fuse.get(strRegExPattern)) {
                        if (fuse.get(strRegExPattern)[0][0] >= 0.7) {
                          event.category = categories[c];
                          console.log(JSON.stringify(categories[c]))

                          console.log(
                            "match catégorie tags : entre " +
                            souscate[s] +
                            " et ",
                            strRegExPattern
                          );
                          flag_match = true;
                        }
                      }
                    }
                  } else {
                    fuse = FuzzySet();
                    fuse.add(categories[c].name.toString());
                    if (fuse.get(strRegExPattern)) {
                      if (fuse.get(strRegExPattern)[0][0] >= 0.7) {
                        event.category = categories[c];
                        console.log(
                          "match catégorie tags : entre " +
                          categories[c].name +
                          " et ",
                          strRegExPattern
                        );
                        flag_match = true;
                      }
                    }
                  }
                }
              }

              //si pas trouvé dans les tags on cherche dans le titre
              if (!flag_match) {
                let matches = event.title.split(" ");

                for (let i = 0; i < matches.length; i++) {
                  // tente de faire un match entre les catégories pour faire l'association
                  // sinon affecter categorie = 'Divers'
                  var strRegExPattern = matches[i].toString().toLowerCase();
                  for (let c = 0; c < categories.length; c++) {
                    let souscate = categories[c].name.split("/");
                    if (souscate) {
                      for (let s = 0; s < souscate.length; s++) {
                        fuse = FuzzySet();
                        fuse.add(souscate[s].toString());
                        if (fuse.get(strRegExPattern)) {
                          if (fuse.get(strRegExPattern)[0][0] >= 0.7) {
                            event.category = categories[c];
                            console.log(
                              "match catégorie title : entre " +
                              souscate[s] +
                              " et ",
                              strRegExPattern
                            );
                          }
                        }
                      }
                    } else {
                      fuse = FuzzySet();
                      fuse.add(categories[c].name.toString());
                      if (fuse.get(strRegExPattern)) {
                        if (fuse.get(strRegExPattern)[0][0] >= 0.7) {
                          event.category = categories[c];
                          console.log(
                            "match catégorie title : entre " +
                            categories[c].name +
                            " et ",
                            strRegExPattern
                          );
                        }
                      }
                    }
                  }
                }
              }

              // creation de l'event
              let returnValue = true;
              console.log("nb resultats = ", eventsZev.nbTotalResults)
              // TODO PAGINER SINON ON A PAS TOUS LES RESULTATS!
              for (let z = 0; z < eventsZev.nbTotalResults; z++) {
                if (eventsZev.nbTotalResults > 0) {
                  if (eventsZev.events[z] != undefined) {
                    if (
                      eventsZev.events[z].id_external == event.id_external
                    ) {
                      console.log("NE PAS INSERER UN DOUBLON");
                      returnValue = false;
                      break;
                    }
                  }
                }
              }

              if (returnValue) {
                console.log("push", event.id_external);
                listodevent.push(event);
              }
            }
            // } // isafter
            // } // datetest
          });
        });
      } // pour chaque departement
    } // pour les 3 mois qui vont venir

    // on randomize la liste:
    listodevent = shuffle(listodevent);

    for (let z = 0; z < listodevent.length; z++) {
      if (
        listodevent[z].id_external &&
        listodevent[z].title &&
        listodevent[z].title.replace(/[^A-Z]/gi, "").length >= 3 &&
        listodevent[z].category &&
        listodevent[z].dates[0].datedebut &&
        listodevent[z].dates[0].datefin &&
        listodevent[z].commune
      ) {
        console.log("insert", listodevent[z].id_external);

        await rp({
          uri: `${ZEV_API}/event`,
          method: "POST",
          headers: {
            Authorization: jwtToken,
            "Content-Type": "application/json"
          },
          json: listodevent[z]
        });
      }
    }
  } catch (e) {
    console.error(e);
  }
};

run();
